<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\OrderLine
 *
 * @property int $id
 * @property string $string
 * @property int $user
 * @property float $order
 * @property string $product
 * @property float $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLine query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLine whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLine whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLine whereProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLine whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLine whereString($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLine whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderLine whereUser($value)
 * @mixin \Eloquent
 */
class OrderLine extends Model
{
    protected $table='order_lines';
}
