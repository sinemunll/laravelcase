<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UserAddress
 *
 * @property int $id
 * @property string $string
 * @property int $user
 * @property string $address
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAddress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAddress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAddress query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAddress whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAddress whereString($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAddress whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAddress whereUser($value)
 * @mixin \Eloquent
 */
class UserAddress extends Model
{
    protected $table='user_address';
}
