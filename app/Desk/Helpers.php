<?php


namespace App\Desk;


use App\Basket;

use App\OrderLine;

use App\Product;
use App\User;
use App\UserAddress;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class Helpers
{
    public static function generateStringKey($length = 30)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function basketCount()
    {
        $count=Basket::whereUser(Auth::user()->id)->count();
        return $count;
    }
    public static function getProductById($product,$type)
    {
        $product=Product::find($product);
        if($product)
        {
            return $product->$type;
        }
        return null;
    }
    public static function getUsersById($user,$type)
    {
        $user=User::find($user);
        if($user)
        {
            return $user->$type;
        }
        return null;
    }
    public static function getUsersAddress()
    {
        $user=UserAddress::whereUser(Auth::user()->id)->get();
        if($user)
        {
            return $user;
        }
    }

    public static function getTotalBasketPrice()
    {
        $basket=Basket::whereUser(Auth::user()->id)->get();
        $total=0;
        foreach ($basket as $bask)
        {
             $total+=self::getProductById($bask->product,'price')*$bask->quantity;
        }
        return $total;

    }
    public static function getOrderLine($order)
    {
        $orderLine=OrderLine::whereOrder($order)->get();
        return $orderLine;
    }


}
