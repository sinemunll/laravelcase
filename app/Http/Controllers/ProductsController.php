<?php

namespace App\Http\Controllers;

use App\Product;
use App\Snm\Helpers;
use Request;

class ProductsController extends Controller
{
    public function index()
    {
        $product=Product::all();
        return view('Products.index')->with(['product'=>$product]);

    }

    public function create()
    {
        return view('Products.Create');
    }
    public function store()
    {
        $code=Request::input('code');
        $description=Request::input('description');
        $name=Request::input('name');
        $price=Request::input('price');


        $product=new Product();
        $product->string=\App\Desk\Helpers::generateStringKey();
        $product->name=$name;
        $product->description=$description;
        $product->code=$code;
        $product->price=$price;
        $product->status=1;
        $product->save();
        return redirect('/Manage/Products');
    }
    public function edit($string)
    {
        $product=Product::whereString($string)->first();
        return view('Products.Edit')->with(['product'=>$product]);
    }
    public function update($string)
    {
        $product=Product::whereString($string)->first();

        $code=Request::input('code');
        $description=Request::input('description');
        $name=Request::input('name');
        $price=Request::input('price');
        $status=Request::input('status');

        $product->name=$name;
        $product->description=$description;
        $product->code=$code;
        $product->price=$price;
        $product->status=$status;
        $product->save();


        return redirect('/Manage/Products');
    }
    public function delete($string)
    {
        Product::whereString($string)->delete();
        return redirect('/Manage/Products');
    }
}
