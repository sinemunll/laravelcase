<?php

namespace App\Http\Controllers;

use App\Basket;
use App\Desk\Helpers;
use App\Order;
use App\OrderLine;
use App\Product;
use App\UserAddress;
use Request;
use Illuminate\Support\Facades\Auth;

class SiteController extends Controller
{
    public function index()
    {
        $product=Product::all();
        return view('home')->with(['product'=>$product]);
    }

    public function basket()
    {
        $basket=Basket::whereUser(Auth::user()->id)->get();
        return view('Basket.index')->with(['basket'=>$basket]);
    }

    public function addBasket($string)
    {
        $product=Product::whereString($string)->first();
        $basket=new Basket();
        $basket->string=Helpers::generateStringKey();
        $basket->product=$product->id;
        $basket->user=Auth::user()->id;
        $basket->quantity=Request::input('quantity');
        $basket->save();
        return redirect('/home');

    }

    public function checkout()
    {
        return view('Basket.CheckOut');

    }

    public function addAdress()
    {
        $user=Auth::user()->id;
        $add=Request::input('address');
        $address=new UserAddress();
        $address->string=Helpers::generateStringKey();
        $address->address=$add;
        $address->user=$user;
        $address->status=1;
        $address->save();
        return redirect('/CheckOut');
    }

    public function orders()
    {
        $orders=Order::whereUser(Auth::user()->id)->get();
        return view('Orders.Main.List')->with(['orders'=>$orders]);
    }
    public function ordersView($string)
    {
        $orders=Order::whereUser(Auth::user()->id)->whereString($string)->first();
        return view('Orders.Main.View')->with(['orders'=>$orders]);
    }

    public function completedOrder()
    {
        $basket=Basket::whereUser(Auth::user()->id)->get();
        $address=Request::input('address');
        $payment=Request::input('payment');

        $order=new Order();
        $order->string=Helpers::generateStringKey();
        $order->user=Auth::user()->id;
        $order->total=Helpers::getTotalBasketPrice();
        $order->address=$address;
        $order->payment_type=$payment;
        $order->save();

        foreach ($basket as $bask)
        {
            $line=new OrderLine();
            $line->string=Helpers::generateStringKey();
            $line->user=Auth::user()->id;
            $line->order=$order->id;
            $line->product=$bask->product;
            $line->quantity=$bask->quantity;
            $line->amount=$bask->quantity*Helpers::getProductById($bask->product,'price');
            $line->save();

        }

        Basket::whereUser(Auth::user()->id)->delete();

        return redirect('/home');

    }
}
