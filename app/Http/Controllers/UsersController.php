<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Request;

class UsersController extends Controller
{
    public function index()
    {
        $users=User::all();
        return view('Users.index')->with(['users'=>$users]);
    }
    public function create()
    {
        return view('Users.Create');
    }

    public function store()
    {
        $name=Request::input('name');
        $email=Request::input('email');
        $password=Request::input('password');
        $auth=Request::input('auth');

        $user=new User();
        $user->name=$name;
        $user->email=$email;
        $user->password = bcrypt($password);
        $user->auth=$auth;
        $user->save();
        return redirect('/Manage/Users');

    }
}
