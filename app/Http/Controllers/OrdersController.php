<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function index()
    {
        $orders=Order::all();
        return view('Orders.index')->with(['orders'=>$orders]);
    }

    public function confirm($string)
    {
        $orders=Order::whereString($string)->first();
        $orders->status=1;
        $orders->save();
        return redirect('/Manage/Orders');
    }
    public function reject($string)
    {
        $orders=Order::whereString($string)->first();
        $orders->status=-1;
        $orders->save();
        return redirect('/Manage/Orders');
    }

    public function view($string)
    {
        $order=Order::whereString($string)->first();
        return view('Orders.View')->with(['order'=>$order]);
    }

}
