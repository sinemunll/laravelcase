<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Basket
 *
 * @property int $id
 * @property string $string
 * @property int $user
 * @property int $product
 * @property int $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket whereProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket whereString($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Basket whereUser($value)
 * @mixin \Eloquent
 */
class Basket extends Model
{
    protected $table='basket';
}
