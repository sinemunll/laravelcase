@extends('layouts.Master')


@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Ürünler</h5>
            <div class="pull-right">
                <a href="{{url('/Manage')}}/Product/Create" class="btn bg-teal-400 btn-labeled btn-rounded"><b><i class="icon-reading"></i></b>
                    Yeni Ürün</a>
            </div>

        </div>
        <div class="panel-body"></div>
        <table class="table  table-hover datatable-basic">
            <thead>
            <tr>
                <th>Kod</th>
                <th>Ürün</th>
                <th>Fiyat</th>
                <th>Durum</th>
                <th class="text-center">İşlem</th>
            </tr>
            </thead>
            <tbody>
            @foreach($product as $pr)
                <tr>
                    <td>{{$pr->code}}</td>
                    <td>{{$pr->name}}</td>
                    <td>{{$pr->price}}</td>
                    <td>@if($pr->status==0)
                            Pasif

                        @else
                            Aktif

                        @endif
                    </td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="text-primary-600">
                                <a href="{{url('/Manage')}}/Product/Edit/{{$pr->string}}"><i class="icon-pencil7"></i></a>
                            </li>
                            <li class="text-danger-600">
                                <a href="{{url('/Manage')}}/Product/Delete/{{$pr->string}}" onclick="return confirm('Silmek istediğinize emin misiniz?');"><i class="icon-trash"></i></a>
                            </li>
                        </ul>
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>

    </div>
@endsection

@section('pageScripts')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{url('/')}}/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/pages/datatables_basic.js"></script>



@endsection


