@extends('layouts.Master')


@section('content')


    <!-- Form horizontal -->
    <div class="panel panel-flat">
        <div class="panel-heading">

        </div>

        <div class="panel-body">
            {!! Form::open(['class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}
            <fieldset class="content-group">
                <legend class="text-bold">Yeni Kullanıcı</legend>

                <div class="form-group">
                    <label class="control-label col-lg-2  text-semibold">Ad Soyad</label>
                    <div class="col-lg-10">
                        <input type="text" name="name" class="form-control">
                    </div>
                </div>
                <div class="form-group" >
                    <label class="control-label col-lg-2  text-semibold">E-posta</label>
                    <div class="col-lg-10">
                        <input type="email" name="email"  class="form-control">
                    </div>
                </div>
                <div class="form-group" >
                    <label class="control-label col-lg-2  text-semibold">Şifre</label>
                    <div class="col-lg-10">
                        <input type="password" name="password"  class="form-control">
                    </div>
                </div>
                <div class="form-group" >
                    <label class="control-label col-lg-2  text-semibold">Yetki </label>
                    <div class="col-lg-10">
                     {!! Form::select('auth',[''=>'Seçiniz','1'=>'Admin','2'=>'Kullanıcı'],0,['class'=>'form-control']) !!}
                    </div>
                </div>

            </fieldset>



            <div class="text-right">
                <button type="submit" class="btn btn-primary">Kaydet <i class="icon-arrow-right14 position-right"></i></button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <!-- /form horizontal -->
@endsection

@section('pageScripts')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{url('/')}}/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/pages/datatables_basic.js"></script>



@endsection


