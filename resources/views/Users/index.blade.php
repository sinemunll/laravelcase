@extends('layouts.Master')


@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Kullanıcılar</h5>
            <div class="pull-right">
                <a href="{{url('/Manage')}}/Users/Create" class="btn bg-teal-400 btn-labeled btn-rounded"><b><i class="icon-reading"></i></b>
                    Yeni Kullanıcı</a>
            </div>

        </div>
        <div class="panel-body"></div>
        <table class="table  table-hover datatable-basic">
            <thead>
            <tr>
                <th>Adı Soyadı</th>
                <th>E-posta</th>
                <th>Yetki</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>@if($user->auth==1)
                            Admin
                        @else
                             Kullanıcı
                        @endif
                    </td>

                </tr>

            @endforeach
            </tbody>
        </table>

    </div>
@endsection

@section('pageScripts')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{url('/')}}/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/pages/datatables_basic.js"></script>



@endsection


