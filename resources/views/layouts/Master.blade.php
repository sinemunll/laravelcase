<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>İçerik Yönetim Sistemi</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{url('/')}}/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="{{url('/')}}/assets/css/components.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{{url('/')}}/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/core/libraries/bootstrap.min.js"></script>

    @yield('HeadScripts')
</head>

<body>
<div class="page-container">
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">

                            <!-- Main -->
                            <li class="navigation-header"><span>Menü</span> <i class="icon-menu" title="Main pages"></i>
                            </li>
                            <li><a href="{{url('/Manage')}}"><i class="icon-home4"></i><span>Anasayfa</span></a></li>
                            <li><a href="{{url('/Manage/Products')}}"><i class="icon-home4"></i><span>Ürünler</span></a></li>
                            <li><a href="{{url('/Manage/Orders')}}"><i class="icon-home4"></i><span>Siparişler</span></a></li>
                            <li><a href="{{url('/Manage/Users')}}"><i class="icon-home4"></i><span>Kullanıcılar</span></a></li>



                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">




            <!-- Content area -->
            <div class="content">


                <!-- Dashboard content -->
                <div class="row">
                    @yield('content')
                </div>


            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
</div>

@yield('pageScripts')

<script type="text/javascript">



</script>
</body>
</html>
