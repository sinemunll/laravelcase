@extends('layouts.Master')


@section('content')


   <div class="row">
       <div class="col-md-12">
           <div class="panel panel-flat">
               <div class="panel-heading">
                   # {{$order->id}} No'lu Sipariş
               </div>

               <div class="panel-body">
                   <table class="table table-striped table-bordered">
                       <thead>
                       <tr>
                           <th scope="col">#</th>
                           <th scope="col">Sipariş No</th>
                           <th scope="col">Ürün</th>
                           <th scope="col">Birim Fiyat</th>
                           <th scope="col">Miktar</th>
                           <th scope="col">Toplam</th>
                       </tr>
                       </thead>
                       <tbody>
                       @foreach(\App\Desk\Helpers::getOrderLine($order->id) as $line)
                       <tr>
                           <th scope="row">{{$line->id}}</th>
                           <td>{{$line->order}}</td>
                           <td>{{App\Desk\Helpers::getProductById($line->product,'name')}}</td>
                           <td>{{number_format(App\Desk\Helpers::getProductById($line->product,'price'),'2',',','.')}}</td>
                           <td>{{$line->quantity}}</td>
                           <td>{{number_format($line->amount,'2',',','.')}}</td>
                       </tr>
                       @endforeach
                       </tbody>
                   </table>



               </div>
           </div>
       </div>

   </div>

    <!-- /form horizontal -->
@endsection

@section('pageScripts')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{url('/')}}/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/pages/datatables_basic.js"></script>



@endsection


