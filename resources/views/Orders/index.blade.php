@extends('layouts.Master')


@section('content')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Siparişler</h5>
        </div>
        <div class="panel-body"></div>
        <table class="table  table-hover datatable-basic">
            <thead>
            <tr>
                <th>Kullanıcı</th>
                <th>Ödeme</th>
                <th>Durum</th>
                <th class="text-center">İşlem</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{App\Desk\Helpers::getUsersById($order->user,'name')}}</td>
                    <td>@if($order->payment_type==1)
                            Kapıda Ödeme
                           @endif
                    </td>
                    <td>@if($order->status==0)
                            Bekliyor

                            @elseif($order->status==1)
                            Onaylandı
                            @else
                            Reddedildi

                        @endif
                    </td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="text-primary-600">
                                <a  title="Detay" href="{{url('/Manage')}}/Orders/View/{{$order->string}}"><i class="icon-eye"></i></a>
                            </li>
                            <li class="text-warning-600">
                                <a  title="Onayla" href="{{url('/Manage')}}/Orders/Confirm/{{$order->string}}"><i class="icon-check"></i></a>
                            </li>
                            <li class="text-danger-600">
                                <a title="Reddet" href="{{url('/Manage')}}/Orders/Reject/{{$order->string}}" onclick="return confirm('Reddetmek istediğinize emin misiniz?');"><i class="icon-trash"></i></a>
                            </li>
                        </ul>
                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>

    </div>
@endsection

@section('pageScripts')
    <!-- Theme JS files -->
    <script type="text/javascript" src="{{url('/')}}/assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/core/app.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/js/pages/datatables_basic.js"></script>



@endsection



