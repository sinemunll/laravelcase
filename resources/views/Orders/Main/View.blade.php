<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    <!-- Document Meta
        ============================================= -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--IE Compatibility Meta-->
    <meta name="author" content="zytheme" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="construction html5 template">


    <link href="{{url('/Main')}}/assets/css/external.css" rel="stylesheet">
    <link href="{{url('/Main')}}/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{url('/Main')}}/assets/css/style.css" rel="stylesheet">
    <link href="{{url('/Main')}}/assets/css/custom.css" rel="stylesheet">


    <script src="{{url('/Main')}}/assets/js/html5shiv.js"></script>
    <script src="{{url('/Main')}}/assets/js/respond.min.js"></script>


    <title>Laravel Case Demo</title>
</head>
<body>
<!-- Document Wrapper
	============================================= -->
<div id="wrapper" class="wrapper clearfix">
    <header id="navbar-spy" class="header header-1">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-5">
                        <ul class="list-inline top-contact">
                            @if(Auth::user()->auth==1)
                                <li><a href="{{url('/Manage')}}">Panel</a></li>
                            @endif
                            <li><a href="{{url('/Orders')}}">Siparişlerim</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-7">
                        <ul class="list-inline pull-right top-links">
                            <li>
                                <a  href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Çıkış
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>

                        </ul>
                    </div>
                </div>
                <!-- .row end -->
            </div>
            <!-- .container end -->
        </div>
        <!-- .top-bar end -->
        <nav id="primary-menu" class="navbar navbar-fixed-top">
            <div class="container">


                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-right" id="header-navbar-collapse-1">
                    <div class="module module-cart pull-left">
                        <div class="cart-icon">
                            <a href="{{url('/Basket')}}"><i class="fa fa-shopping-cart"></i></a>
                            <span class="cart-label">{{App\Desk\Helpers::basketCount()}}</span>
                        </div>
                    </div>


                </div>

            </div>

        </nav>
    </header>


    <section id="featuredItems" class="shop">

        <div class="container">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h2>Siparişlerim</h2>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Sipariş No</th>
                                <th scope="col">Ürün</th>
                                <th scope="col">Birim Fiyat</th>
                                <th scope="col">Miktar</th>
                                <th scope="col">Toplam</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\Desk\Helpers::getOrderLine($orders->id) as $line)
                                <tr>
                                    <th scope="row">{{$line->id}}</th>
                                    <td>{{$line->order}}</td>
                                    <td>{{App\Desk\Helpers::getProductById($line->product,'name')}}</td>
                                    <td>{{number_format(App\Desk\Helpers::getProductById($line->product,'price'),'2',',','.')}}</td>
                                    <td>{{$line->quantity}}</td>
                                    <td>{{number_format($line->amount,'2',',','.')}}</td>
                                </tr>
                            @endforeach
                        </table>

                    </div>
                </div>


            </div>



        </div>

    </section>






</div>

<script src="{{url('/Main')}}/assets/js/jquery-2.2.4.min.js"></script>
<script src="{{url('/Main')}}/assets/js/plugins.js"></script>
<script src="{{url('/Main')}}/assets/js/functions.js"></script>
</body>
</html>
