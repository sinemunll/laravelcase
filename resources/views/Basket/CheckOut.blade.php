<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--IE Compatibility Meta-->
    <meta name="author" content="zytheme" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="construction html5 template">
    <link href="{{url('/Main')}}/assets/css/external.css" rel="stylesheet">
    <link href="{{url('/Main')}}/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{url('/Main')}}/assets/css/style.css" rel="stylesheet">
    <link href="{{url('/Main')}}/assets/css/custom.css" rel="stylesheet">

    <script src="{{url('/Main')}}/assets/js/html5shiv.js"></script>
    <script src="{{url('/Main')}}/assets/js/respond.min.js"></script>

    <title>Laravel Case Demo</title>
</head>
<body>

	============================================= -->
<div id="wrapper" class="wrapper clearfix">
    <header id="navbar-spy" class="header header-1">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-5">
                        <ul class="list-inline top-contact">
                            @if(Auth::user()->auth==1)
                                <li><a href="{{url('/Manage')}}">Panel</a></li>
                            @endif
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-7">
                        <ul class="list-inline pull-right top-links">
                            <li>
                                <a  href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Çıkış
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- .top-bar end -->
        <nav id="primary-menu" class="navbar navbar-fixed-top">
            <div class="container">


                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-right" id="header-navbar-collapse-1">
                    <div class="module module-cart pull-left">
                        <div class="cart-icon">
                            <a href="{{url('/Basket')}}"><i class="fa fa-shopping-cart"></i></a>
                            <span class="cart-label">{{App\Desk\Helpers::basketCount()}}</span>
                        </div>
                    </div>


                </div>

            </div>

        </nav>
    </header>


    <section id="featuredItems" class="shop">
{!! Form::open(['url'=>'/Order/Complete']) !!}
        <div class="container">
            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-header">
                        Adres  <div class="pull-right"><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#address">ekle</button></div>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Seç</th>
                                <th>Adres</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\Desk\Helpers::getUsersAddress() as $user)
                            <tr>
                                <td class="text-center"><input type="radio"  required="" name="address" value="{{$user->string}}"></td>
                                <td>{{$user->address}}</td>

                            </tr>
                            @endforeach


                            </tbody>

                        </table>

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-header">
                        Ödeme Seçenekleri
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::select('payment',[''=>'Seçiniz','1'=>'Kapıda Ödeme'],0,['class'=>'form-control']) !!}
                        </div>


                        <table  width="40%">
                            <tbody><tr>
                                <td>Toplam :</td>
                                <td id="genel_toplam"><b>{{number_format(\App\Desk\Helpers::getTotalBasketPrice(),'2',',','.')}}</b> TL</td>
                            </tr>
                            </tbody></table>
                    </div>

                </div>

            </div>

            <button  class="btn btn-primary pull-right" type="submit">Siparişi Tamamla </button>


        </div>

     {!! Form::close() !!}

    </section>


    <div class="modal fade" id="address" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-full">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Adres Ekle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['url'=>'Address/Add']) !!}
                <div class="modal-body">
                    <label>Adres</label>
                    <textarea name="address" rows="9" cols="9" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                    <button type="submit" class="btn btn-primary">Kaydet</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>




</div>

<script src="{{url('/Main')}}/assets/js/jquery-2.2.4.min.js"></script>
<script src="{{url('/Main')}}/assets/js/plugins.js"></script>
<script src="{{url('/Main')}}/assets/js/functions.js"></script>
</body>
</html>

