<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('string');
            $table->integer('user');
            $table->decimal('total');
            $table->text('address');
            $table->timestamps();
        });
        Schema::create('order_lines', function (Blueprint $table) {
            $table->id();
            $table->string('string');
            $table->integer('user');
            $table->integer('order');
            $table->text('product');
            $table->integer('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
