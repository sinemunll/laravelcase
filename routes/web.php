<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'SiteController@index')->name('home');

Route::get('/Basket', 'SiteController@basket');
Route::post('/Add/Basket/{string}', 'SiteController@addBasket');
Route::get('/CheckOut', 'SiteController@checkout');
Route::post('/Address/Add', 'SiteController@addAdress');
Route::get('/Orders', 'SiteController@orders');
Route::get('/Orders/View/{string}', 'SiteController@ordersView');
Route::post('/Order/Complete', 'SiteController@completedOrder');


Route::get('/Manage', 'HomeController@manage');

/* Products*/

Route::get('/Manage/Products', 'ProductsController@index');
Route::get('/Manage/Product/Create', 'ProductsController@create');
Route::post('/Manage/Product/Create', 'ProductsController@store');
Route::get('/Manage/Product/Edit/{string}', 'ProductsController@edit');
Route::post('/Manage/Product/Edit/{string}', 'ProductsController@update');
Route::get('/Manage/Product/Delete/{string}', 'ProductsController@delete');

/* Orders*/

Route::get('/Manage/Orders', 'OrdersController@index');
Route::get('/Manage/Orders/Confirm/{string}', 'OrdersController@confirm');
Route::get('/Manage/Orders/Reject/{string}', 'OrdersController@reject');
Route::get('/Manage/Orders/View/{string}', 'OrdersController@view');

/* Users*/

Route::get('/Manage/Users', 'UsersController@index');
Route::get('/Manage/Users/Create', 'UsersController@create');
Route::post('/Manage/Users/Create', 'UsersController@store');
